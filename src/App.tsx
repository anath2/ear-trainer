import logo from './logo.svg'
import './App.scss'
import { NavLink, Outlet } from 'react-router-dom'
import { letters, intervals } from './constants.js'
import NoteListening from './page/note-listening'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* <p>Hello Vite + React!</p> */}
        <p>Ear Trainer</p>
      </header>
      <nav className="App-nav">
        <ul className="menu">
          <li><NavLink
            style={({ isActive }) => {
              return {
                color: isActive ? "#d94f5c" : "",
              };
            }}
            to='/note-listening'>Note</NavLink>
          </li>
          <li>
            <NavLink
              style={({ isActive }) => {
                return {
                  color: isActive ? "#d94f5c" : "",
                };
              }}
              to='/interval-listening'>Interval</NavLink>
          </li>
        </ul>
      </nav>
      <section className="App-header">
        <Outlet />
      </section>
    </div>
  )
}

export default App

