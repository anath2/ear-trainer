import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import App from './App'
import NoteListening from './page/note-listening'
import IntervalListening from './page/interval-listening'
import './index.css'

console.log(import.meta.env.BASE_URL);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <BrowserRouter basename={import.meta.env.BASE_URL}>
      <Routes>
        <Route path="/" element={<App />}>
          <Route
            index
            element={
              <Navigate replace to="/note-listening" />
            }
          />
          <Route path="/note-listening" element={<NoteListening />} />
          <Route path="/interval-listening" element={<IntervalListening />} />
          <Route
            path="*"
            element={
              <main style={{ padding: "1rem" }}>
                <p>四零四</p>
                <p>Go Home, You Are Drunk</p>
              </main>
            }
          />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
)
