import { useState } from 'react'
import * as Tone from 'tone'
import { letters, intervals } from '../constants.js'

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
 function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const IntervalListening = () => {
  const [letterNumber, setLetterNumber] = useState(-1);
  const [showAnswer, setShowAnswer] = useState(false);
  const [started, setStarted] = useState(false);

  const synth = new Tone.Synth().toDestination()

  let now: number

  const playCadence = () => {
    const synth4 = new Tone.PolySynth(Tone.Synth).toDestination();
    now = Tone.now()
    const increment = 0.5
    const duration = 0.3
    synth4.triggerAttackRelease(["C4", "E4", "G4"], duration, now);
    synth4.triggerAttackRelease(["C4", "F4", "A4"], duration, now + increment);
    synth4.triggerAttackRelease(["B3", "D4", "G4"], duration, now + 2 * increment);
    synth4.triggerAttackRelease(["C4", "E4", "G4"], duration, now + 3 * increment);
    now =  now + 4 * increment;
  }

  const genNextQuestion = () => {
    // playCadence()
    now = Tone.now()
    setShowAnswer(false)
    const letterNumber = Math.floor(Math.random() * letters.length)
    setLetterNumber(letterNumber) // pick random letter from letters)
    playInterval(letterNumber)
  }

  const playInterval = (letterNumber: number) => {
    const letter = letters[letterNumber]

    synth.triggerAttackRelease("C4", "8n", now + 0.2);
    synth.triggerAttackRelease(letter + "4", "8n", now + 0.7);
    console.log({ letter });
    now = now + 1.2
  }

  const repeatInterval = () => {
    now = Tone.now()
    playInterval(letterNumber)
  }

  return ( 
    <div>
        {
          (!started) ?
            <button onClick={(e) => { setStarted(true); genNextQuestion() }}>Start</button>
            : null
        }
        {(started) ?
          <div>
            <div>
              <button onClick={(e) => { genNextQuestion() }}>Next Question</button>
              <button onClick={(e) => { repeatInterval() }}>Repeat Note</button>
            </div>
            <div><button onClick={(e) => { setShowAnswer(true) }}>Show Answer</button></div>
            <p>
              {showAnswer && (
                'Answer: ' + (intervals.find(
                  interval => interval.distance === letterNumber)?.name
                  ?? '')
              )
              }
              {!showAnswer && '---'}
            </p>
          </div>

          : null
        }
        </div>
   );
}
 
export default IntervalListening;